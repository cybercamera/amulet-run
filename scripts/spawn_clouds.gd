extends Node2D

var cloud = preload("res://scenes/cloud.tscn")

var cloud1_texture = preload("res://sprites/clouds/clouds1_1.png")
var cloud2_texture = preload("res://sprites/clouds/clouds2_0.png")
var cloud3_texture = preload("res://sprites/clouds/clouds3_0.png")
var darkcloud1_texture = preload("res://sprites/clouds/darkclouds1_0.png")
var darkcloud2_texture = preload("res://sprites/clouds/darkclouds2_0.png")
var darkcloud3_texture = preload("res://sprites/clouds/darkclouds3_0.png")
var lightningcloud1_texture = preload("res://sprites/clouds/lightningcloud1.png")
var lightningcloud2_texture = preload("res://sprites/clouds/lightningcloud2.png")
var lightningcloud3_texture = preload("res://sprites/clouds/lightningcloud3.png")

func _ready():
	randomize()
	get_node("timer").set_wait_time(global.cloud_generation_delay)
	#We need to establish a direction for the wind
	var x_vel = rand_range(-5, 5)
	var y_vel = rand_range(-5, 5)
	global.wind_direction=Vector2(x_vel, y_vel)
	print("Wind directionx x: ", global.wind_direction.x)
	print("Wind directionx y: ", global.wind_direction.y)

func spawn_clouds():
	var a
	var x_pos
	var y_pos
	for i in range(randi()%global.cloud_spawn_count + 1):
		if global.wind_direction.x < 0:
			#clouds moving left, thus we spawn clouds right
			x_pos = global.viewport_size.x + rand_range(-10, 30*global.wind_direction.x)
		else:
			#clouds moving right. thus we spawm clouds left
			x_pos = 0 - rand_range(-30*global.wind_direction.x, 10)
		if global.wind_direction.y < 0:
			#clouds moving up, thus we spawn clouds from bottom
			y_pos = global.viewport_size.y + rand_range(-10, 30*global.wind_direction.y)
		else:
			#clouds moving down, thus we spawn clouds from top
			y_pos = 0 + rand_range(-30*global.wind_direction.y, 10)
		#Let's make the cloud
		a = cloud.instance()
		#Let's set the random cloud sprite
		var j = randi()%9
		if j == 0:
			a.get_node("sprite").set_texture(cloud1_texture)
		elif j == 1:
			a.get_node("sprite").set_texture(cloud2_texture)
		elif j == 2:
			a.get_node("sprite").set_texture(cloud3_texture)
		elif j == 3:
			a.get_node("sprite").set_texture(darkcloud1_texture)
		elif j == 4:
			a.get_node("sprite").set_texture(darkcloud2_texture)
		elif j == 5:
			a.get_node("sprite").set_texture(darkcloud3_texture)
		elif j == 6:
			a.get_node("sprite").set_texture(lightningcloud1_texture)
		elif j == 7:
			a.get_node("sprite").set_texture(lightningcloud2_texture)
		else:
			a.get_node("sprite").set_texture(lightningcloud3_texture)
		#print(get_parent().get_node("player").get_position_in_parent())
		a.set_global_pos(Vector2(x_pos, y_pos) + _get_camera_center())
		add_child(a)
		print(a.get_name())
		print(x_pos, " ", y_pos)

func _on_timer_timeout():
	print("Spawn clouds")
	spawn_clouds()

func _get_camera_center():
    var vtrans = get_canvas_transform()
    var top_left = -vtrans.get_origin() / vtrans.get_scale()
    var vsize = get_viewport_rect().size
    return top_left + 0.5*vsize/vtrans.get_scale()