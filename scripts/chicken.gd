extends KinematicBody2D

var vel=Vector2()
var pos = Vector2()
var magnitude = 3
var movement = 0

func _ready():
	randomize()
	get_node("timer").set_wait_time(global.chicken_timer)
	pos = get_pos()
	set_process(true)
	
func _process(delta):
	movement = magnitude*vel*delta
	if abs(vel.x) > 2:
		# only run this code if the chicken is moving fast enough
		if abs(vel.x) > abs (vel.y):
			if vel.x <  0:
				get_node("animated_sprite").set_animation("walk_left")
				get_node("animated_sprite").play()
			elif vel.x > 0:
				get_node("animated_sprite").set_animation("walk_right")
				get_node("animated_sprite").play()
		else:
			if vel.y < 0:
				get_node("animated_sprite").set_animation("walk_up")
				get_node("animated_sprite").play()
			elif vel.y > 0:
				get_node("animated_sprite").set_animation("walk_down")
				get_node("animated_sprite").play()
		pos += movement
		set_pos(pos + movement)
	else:
		if abs(vel.x) > abs (vel.y):
			if vel.x <  0:
				get_node("animated_sprite").set_animation("eat_left")
				get_node("animated_sprite").play()
			elif vel.x > 0:
				get_node("animated_sprite").set_animation("eat_right")
				get_node("animated_sprite").play()
		else:
			if vel.y < 0:
				get_node("animated_sprite").set_animation("eat_up")
				get_node("animated_sprite").play()
			elif vel.y > 0:
				get_node("animated_sprite").set_animation("eat_down")
				get_node("animated_sprite").play()
	

func _on_timer_timeout():
	var x_vel = rand_range(-3, 3)
	var y_vel = rand_range(-3, 3)
	vel=Vector2(x_vel, y_vel)
	if rand_range(-1, 1) > 0:
		get_node("sound").play("chicken")
	else:
		get_node("sound").play("rooster")

func _on_VisibilityEnabler2D_enter_screen():
	get_node("timer").start()

func _on_VisibilityEnabler2D_exit_screen():
	get_node("timer").stop()
