extends KinematicBody2D

onready var effect = get_node("effect")
onready var sprite = get_node("sprite")
onready var health_bar = get_node("health_bar")
onready var death_timer = get_node("death_timer")

var fire_scene = preload("res://scenes/fire.tscn")
signal explode

var health = global.egg_health
var points = global.egg_points

var vel=Vector2()
var pos = Vector2()
var magnitude = 3
var movement = 0

func _ready():
	randomize()
	get_node("timer").set_wait_time(global.egg_timer)
	pos = get_pos()
	add_to_group("enemies")
	add_to_group("eyeball")
	effect.interpolate_property(sprite, "transform/scale", sprite.get_scale(), Vector2(2,2), 0.3, Tween.TRANS_BOUNCE, Tween.EASE_OUT)
	effect.interpolate_property(sprite, "visibility/opacity", 1, 0.5, 0.3, Tween.TRANS_BOUNCE, Tween.EASE_OUT)
	set_process(true)
	
func _process(delta):
	show_health()
	movement = magnitude*vel*delta
	pos += movement
	set_pos(pos + movement)

func show_health():
	var health_level = health_bar.get_value()
	var colour = "green"
	if health_level < 30:
		colour = "red"
	elif  health_level < 60:
		colour = "yellow"
	var texture = load("res://art/gui/barHorizontal_%s_mid 200.png" % colour)
	health_bar.set_progress_texture(texture)
	health_bar.set_value(float(health)/global.eyeball_health*100)

func _on_timer_timeout():
	var x_vel = rand_range(-5, 5)
	var y_vel = rand_range(-5, 5)
	vel=Vector2(x_vel, y_vel)
	make_noise()

func make_noise():
	get_node("sound").play("eye")

func _on_VisibilityEnabler2D_enter_screen():
		get_node("timer").start()

func _on_VisibilityEnabler2D_exit_screen():
	get_node("timer").stop()

func damage(amount):
	health -= amount
	if health <= 0:
		global.score += global.ghost_points
		connect("explode", self, "explode")
		emit_signal("explode", get_global_pos())

func explode(pos):
	effect.start()

func _on_effect_tween_complete( object, key ):
	var fire = fire_scene.instance()
	fire.set_pos(pos)
	get_parent().add_child(fire)
	global.score += global.egg_points
	queue_free()

func _on_area_body_enter( body ):
	if body.get_groups().has("player"):
		var target_pos = body.get_global_pos()
		var pos = get_global_pos()
		var target_dir = target_pos - pos
		target_dir = target_dir.normalized()
		vel = Vector2(target_dir.x, target_dir.y) * global.egg_speed

func _on_area_body_exit( body ):
	_on_area_body_enter(body)

func _on_VisibilityEnabler2D_enter_viewport( viewport ):
	# As the enemy has entered the viewport, stop the timer from releasing it 
	death_timer.stop()

func _on_VisibilityEnabler2D_exit_viewport( viewport ):
	# As the enemy has left the viewport, set the timer to release it 
	death_timer.start()

func _on_death_timer_timeout():
	# Enemy has been out of view for long enough. Release it.
	print("Freeing egg")
	queue_free()