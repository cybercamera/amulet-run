extends KinematicBody2D

var fireball_scene = preload("res://scenes/fireball.tscn")

onready var Hermione = get_node("Hermione")
onready var gun_timer = get_node("gun_timer")
onready var sound = get_node("sound")


var shield_up = true

# Member variables
const MOTION_SPEED = 160 # Pixels/second

var anim = "Idle"
var count = 0

func _fixed_process(delta):
	var motion = Vector2()
	anim = "Idle"
	if (Input.is_action_pressed("ui_up")):
		motion += Vector2(0, -1)
		anim = "WalkingUp"
		walk_sound()
	if (Input.is_action_pressed("ui_down")):
		motion += Vector2(0, 1)
		anim = "WalkingDown"
		walk_sound()
	if (Input.is_action_pressed("ui_left")):
		motion += Vector2(-1, 0)
		anim = "WalkingLeft"
		walk_sound()
	if (Input.is_action_pressed("ui_right")):
		motion += Vector2(1, 0)
		anim = "WalkingRight"
		walk_sound()
	Hermione.set_animation(anim)
	if (Input.is_action_pressed("fireball")):
		print(gun_timer.get_time_left())
		if gun_timer.get_time_left() == 0:
			shoot_fireball()
	
	motion = motion.normalized()*MOTION_SPEED*delta
	motion = move(motion)
	
	# Make character slide nicely through the world
	var slide_attempts = 4
	while(is_colliding() and slide_attempts > 0):
		motion = get_collision_normal().slide(motion)
		motion = move(motion)
		slide_attempts -= 1

func walk_sound():
	if count > 30:
		get_node("../../SoundFX").play("Footstep_Dirt_02")
		count = 0

func _ready():
	add_to_group("player")
	set_fixed_process(true)
	
func shoot_fireball():
	gun_timer.start()
	var target_pos = get_global_mouse_pos()
	var pos = get_global_pos()
	# print(target_pos)
    # var dir = (target_obj.get_transform().origin - get_transform().origin).normalized()
	var target_dir = target_pos - pos
	target_dir = target_dir.normalized()
	# print(target_dir)
	var fireball = fireball_scene.instance()
	fireball.speed_x = target_dir.x 
	#print(fireball.speed_x)
	fireball.speed_y = target_dir.y
	#print(fireball.speed_y)
	get_parent().add_child(fireball)
	fireball.set_pos(get_pos())
	sound.play("fireball")
