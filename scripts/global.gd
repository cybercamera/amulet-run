extends Node

# game settings
var game_over = false
var score = 0
var level = 0
var paused = false
var current_scene = null
var new_scene = null
var map_size = Vector2(1000,1000) #define a set initial size, which will be set to each map later
var viewport_size = Vector2()

# player settings
const HEALTH_MAX = 100
var health_regen = 10
var health_regen_time = 10
var fireball_damage = 10
var health_points = HEALTH_MAX

# NPC timers
var sheep_timer = 17
var brown_horse_timer = 19
var white_horse_timer = 23
var ghost_timer = 29
var eyeball_timer = 31
var egg_timer = 37
var cow_timer = 41
var chicken_timer = 43
var zombie_timer = 47
var grue_timer = 51
var skeleton_timer = 53

# enemy settings
var enemy_spawn_delay = 60 # How long to wait until additional enemies are spawned in this scene
var enemy_spawn_count = 5 # How many new enemies to spawn on each cycle


var ghost_bullet_damage = 25
var ghost_damage = 25
var ghost_health = 100
var ghost_points = 100
var ghost_speed = 10
var ghost_regeneration = 30

var egg_bullet_damage = 25
var egg_damage = 15
var egg_health = 50
var egg_points = 100
var egg_speed = 5
var egg_regeneration = 10

var eyeball_bullet_damage = 25
var eyeball_damage = 15
var eyeball_health = 30
var eyeball_points = 100
var eyeball_speed = 10
var eyeball_regeneration = 10

var grue_bullet_damage = 25
var grue_damage = 30
var grue_health = 150
var grue_points = 150
var grue_speed = 30
var grue_regeneration = 20

var skeleton_bullet_damage = 25
var skeleton_damage = 20
var skeleton_health = 70
var skeleton_points = 100
var skeleton_speed = 10
var skeleton_regeneration = 10

var zombie_bullet_damage = 25
var zombie_damage = 25
var zombie_health = 80
var zombie_points = 100
var zombie_speed = 15
var zombie_regeneration = 10

# Environment settings
var cloud_generation_delay = 10
var cloud_spawn_count = 3
var wind_direction = Vector2()

# asteroid settings
var break_pattern = {'big': 'med',
					 'med': 'sm',
					 'sm': 'tiny',
					 'tiny': null}

var ast_damage = {'big': 40,
			  	  'med': 20,
		      	  'sm': 15,
			  	  'tiny': 10}

var ast_points = {'big': 10,
			  	  'med': 15,
		      	  'sm': 20,
			  	  'tiny': 40}

func _ready():
	var root = get_tree().get_root()
	current_scene = root.get_child(root.get_child_count() - 1)

func goto_scene(path):
	var s = ResourceLoader.load(path)
	new_scene = s.instance()
	get_tree().get_root().add_child(new_scene)
	get_tree().set_current_scene(new_scene)
	current_scene.queue_free()
	current_scene = new_scene

func new_game():
	game_over = false
	score = 0
	level = 0 
	health_points = HEALTH_MAX
	goto_scene("res://scenes/map1.tscn")
	