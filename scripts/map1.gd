extends Node2D

var spawn_locations = preload("res://scenes/spawn_locations.tscn")
var ghost = preload("res://scenes/ghost.tscn")

onready var HUD = get_node("HUD")
onready var spawn_clouds = preload("res://scenes/spawn_clouds.tscn")

func _ready():
	randomize()
	global.map_size = get_node("background").get_region_rect().size
	global.viewport_size = get_viewport().get_rect().size
	print(get_viewport().get_rect().size)
	#spawn_enemies()
	set_process(true)
	HUD.show_message("Amulet Run")
	
func _process(delta):
	HUD.update()
	
func spawn_ghost():
	var a = ghost.instance()
	add_child(a)
	var size = get_node("ghost").get_size()
	var pos = Vector2(50, 50)
	a.init(size, pos, Vector2(0,0))

#func spawn_enemies():
#	var a = spawn_locations.instance()
#	add_child(a)
