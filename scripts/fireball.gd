extends Area2D

export var speed_x = 1
export var speed_y = 0

onready var effect = get_node("effect")
onready var sprite = get_node("particles")

var fireball_speed = 500

func _ready():
	add_to_group("fireball")
	effect.interpolate_property(sprite, "config/half_extents", Vector2(2,2), Vector2(45,45), 0.3, Tween.TRANS_BOUNCE, Tween.EASE_OUT)
	effect.interpolate_property(sprite, "visibility/opacity", 1, 0.5, 0.3, Tween.TRANS_BOUNCE, Tween.EASE_OUT)
	set_process(true)
	
func _process(delta):
	var motion = Vector2(speed_x, speed_y) * fireball_speed
	set_pos(get_pos() + motion*delta)

func _on_fireball_body_enter( body ):
	if body.get_groups().has("enemies"):
		effect.start()
		fireball_speed = 50
		body.damage(global.fireball_damage)

func _on_effect_tween_complete( object, key ):
	queue_free()
