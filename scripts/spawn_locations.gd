extends Node2D

onready var timer = get_node("timer")

var egg = preload("res://scenes/egg.tscn")
var eyeball = preload("res://scenes/eyeball.tscn")
var ghost = preload("res://scenes/ghost.tscn")
var grue = preload("res://scenes/grue.tscn")
var skeleton = preload("res://scenes/skeleton.tscn")
var zombie = preload("res://scenes/zombie.tscn")


func _ready():
	randomize()
	timer.set_wait_time(global.enemy_spawn_delay)
	timer.start()
	#spawn_enemies()

func spawn_enemies():
	var a
	var x_pos
	var y_pos
	for i in range(global.enemy_spawn_count):
		if randi()%2 < 1:
			#spawn to right of screen
			x_pos = global.viewport_size.x + rand_range(-150, 350)
		else:
			#spawn to left
			x_pos = 0 + rand_range(-350, 150)
		if randi()%2 < 1:
			#spawn to bottom
			y_pos = global.viewport_size.y + rand_range(-150, 350)
		else:
			#spawn to top
			y_pos = 0 + rand_range(-350, 150)
		var j = randi()%6
		if j == 0:
			a = egg.instance()
		elif j == 1:
			a = eyeball.instance()
		elif j == 2:
			a = ghost.instance()
		elif j == 3:
			a = grue.instance()
		elif j == 4:
			a = skeleton.instance()
		else:
			a = zombie.instance()
			
		a.set_global_pos(Vector2(x_pos, y_pos) + _get_camera_center())
		add_child(a)
		print(a.get_name())
		print(x_pos, y_pos)

func _on_timer_timeout():
	spawn_enemies()

func _get_camera_center():
    var vtrans = get_canvas_transform()
    var top_left = -vtrans.get_origin() / vtrans.get_scale()
    var vsize = get_viewport_rect().size
    return top_left + 0.5*vsize/vtrans.get_scale()