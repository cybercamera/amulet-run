extends KinematicBody2D

signal death
var fireball_scene = preload("res://scenes/fireball.tscn")

onready var Hermione = get_node("Hermione")
onready var fireball_timer = get_node("gun_timer")
onready var death_timer = get_node("death_timer")
onready var sound = get_node("sound")
onready var hit_puff = get_node("hit_puff")
onready var regeneration_timer = get_node("regeneration_timer")

# Member variables
const MOTION_SPEED = 160 # Pixels/second

var anim = "Idle"
var count = 0

func _fixed_process(delta):
	var motion = Vector2()
	anim = "Idle"
	if (Input.is_action_pressed("ui_up")):
		motion += Vector2(0, -1)
		anim = "WalkingUp"
		walk_sound()
	if (Input.is_action_pressed("ui_down")):
		motion += Vector2(0, 1)
		anim = "WalkingDown"
		walk_sound()
	if (Input.is_action_pressed("ui_left")):
		motion += Vector2(-1, 0)
		anim = "WalkingLeft"
		walk_sound()
	if (Input.is_action_pressed("ui_right")):
		motion += Vector2(1, 0)
		anim = "WalkingRight"
		walk_sound()
	Hermione.set_animation(anim)
	if (Input.is_action_pressed("fireball")):
		if fireball_timer.get_time_left() == 0:
			shoot_fireball()
	
	motion = motion.normalized()*MOTION_SPEED*delta
	motion = move(motion)
	
	# Make character slide nicely through the world
	var slide_attempts = 4
	while(is_colliding() and slide_attempts > 0):
		motion = get_collision_normal().slide(motion)
		motion = move(motion)
		slide_attempts -= 1

func walk_sound():
	if count > 30:
		get_node("../../SoundFX").play("Footstep_Dirt_02")
		count = 0

func _ready():
	add_to_group("player")
	set_fixed_process(true)
	regeneration_timer.set_wait_time(global.health_regen_time)
	
func shoot_fireball():
	fireball_timer.start()
	var target_pos = get_global_mouse_pos()
	var pos = get_global_pos()
	var target_dir = target_pos - pos
	target_dir = target_dir.normalized()
	var fireball = fireball_scene.instance()
	fireball.speed_x = target_dir.x 
	fireball.speed_y = target_dir.y
	get_parent().add_child(fireball)
	fireball.set_pos(get_pos())
	sound.play("fireball")

func _on_area_body_enter( body ):
	if is_hidden():
		return
	if body.get_groups().has("enemies"):
		hit_puff.set_emitting(true)
		if body.get_groups().has("egg"):
			damage(global.egg_damage)
		if body.get_groups().has("eyeball"):
			damage(global.eyeball_damage)
		if body.get_groups().has("ghost"):
			damage(global.ghost_damage)
		if body.get_groups().has("grue"):
			damage(global.grue_damage )
		if body.get_groups().has("skeleton"):
			damage(global.skeleton_damage)
		if body.get_groups().has("zombie"):
			damage(global.zombie_damage)
	
func damage(damage_amount):
	global.health_points -= damage_amount
	if global.health_points <= 0:
		#emit_signal("death")
		sound.play("heartbeat_long")
		death_timer.start()
		player_death()
	else:
		sound.play("heartbeat")

func player_death():
	Hermione.set_animation("Death")
	set_fixed_process(false)

func _on_death_timer_timeout():
	Hermione.stop()
	hide()
	global.new_game()

func _on_regeneration_timer_timeout():
	# every regeneration click, we bump up the player's health points
	global.health_points += global.health_regen
	if global.health_points > global.HEALTH_MAX:
		# if we're over the limit, set it back to the limit
		global.health_points = global.HEALTH_MAX
 
