extends Node2D

onready var death_timer = get_node("death_timer")

var pos = Vector2()
var magnitude = 0
var movement = 0

func _ready():
	randomize()
	pos = get_pos()
	magnitude = rand_range(1, 6)
	set_process(true)
	
func _process(delta):
	movement = magnitude*global.wind_direction*delta
	pos += movement
	set_pos(pos + movement)
	#print(pos)
	
func _on_VisibilityEnabler2D_enter_viewport( viewport ):
	# As the enemy has entered the viewport, stop the timer from releasing it 
	death_timer.stop()

func _on_VisibilityEnabler2D_exit_viewport( viewport ):
	# As the enemy has left the viewport, set the timer to release it 
	death_timer.start()

func _on_death_timer_timeout():
		# Enemy has been out of view for long enough. Release it.
	print("Freeing cloud")
	queue_free()